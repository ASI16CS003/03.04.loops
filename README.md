Name:ABHIMANYU C V
Roll No:03
Name and Number of the Experiment: EXP 3
Date:09-02-18

SOURCE CODE :
echo "Process Management..."
ps aux | sort -nrk 3 | head | awk '{print $2 " " $3 " " $11 }'> psout
cat psout
read -p "Enter Threshold Level: " th
while IFS= read line
do
pno=$(echo $line | awk '{print $1}')
cpuload=$(echo $line | awk '{print $2}')
pname=$(echo $line | awk '{print $3}')
if [ $(echo "$cpuload >= $th" | bc -l) -eq 1 ]
then
echo "$pno $cpuload $pname"
kill $pno
fi
done <"psout" 

